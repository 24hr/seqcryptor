# seqCryptor

This is a  small module to encrypt specififc columns with seqeuelize.
It's a little like https://www.npmjs.com/package/sequelize-encrypted but since seqeuelize-encrypted hasnt been updated
for five years and we needed a typescript version, we deciced to create a new module.

It's just 80+ rows of code, so nothing fancy, but our goals was to create a module that we could use in procjects
that need to encrypt data AFTER the project had grown a lot. So this is designed with the goal of chaning as little as possbile
to the normal model definition in sequelize.

It currently only supports ```DataType.STRING```, ```DataType.JSONB``` and ```DataType.JSON```.

## How to use it

This is the most basic example:

```typescript

// Create the encryptor with your secret key
const encryptor = seqEncryptor('YOUR_SUPER_SECRET_32_BYTES_KEY__');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: ':memory:',
    define: {
        timestamps: true,
    },
    logging: false,
});

class User extends Model {
    public name!: string;
    public guid!: string;
}

User.init({
    guid: {
        allowNull: false,
        unique: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
    },
    name: encryptor({               // THIS IS THE ONLY DIFFERENCE. Together with the field below.
        type: DataTypes.STRING,
        field: 'name',              // THIS IS REQUIRED. The initiation functions doens't have access to the name in the json,
                                    // so we need to explicitly write it.
    }),
}, {
    sequelize,
    paranoid: false,
});

await User.sync();

const user = await User.create({ name: 'bar' });

const rawUser = await User.findOne({ where: { guid: user.guid }, raw: true });
console.log(rawUser.name); // Will print an encrypted string

const loadedUser = await User.findOne({ where: { guid: user.guid } });
console.log(loadedUser.name); // Will print "bar"

```


## How to test it

To run the tests, run:

```bash
npm ci
npm run test-ci
```




