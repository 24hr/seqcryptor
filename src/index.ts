import crypto from 'crypto';

import { DataTypes } from 'sequelize';
import { ModelAttributeColumnOptions } from 'sequelize/types';

const algorithm = 'aes-256-cbc';

const ivSeparator = ']'; // Not a base64 character

export const seqCryptor = (secret: string) => {

    if (Buffer.byteLength(secret, 'utf8') < 32) {
        throw new Error(`Secret must be 64 bytes, got ${Buffer.byteLength(secret, 'utf8')}`);
    }

    function field(options: ModelAttributeColumnOptionsWithName): ModelAttributeColumnOptions {

        if (options.type !== DataTypes.STRING && options.type !== DataTypes.JSON && options.type !== DataTypes.JSONB) {
            throw new Error('Data type not supported. Currently only STRING, JSON and JSONB are supported');
        }

        if (options.baseType && options.baseType !== DataTypes.STRING && options.baseType !== DataTypes.TEXT) {
            throw new Error('baseType needs to be STRING or TEXT');
        }

        return {
            ...options,
            type: options.baseType || DataTypes.TEXT,
            get () {
                const rawData = this.getDataValue(options.field);
                const decryptedData = decrypt(secret, rawData, options.returnRawOnUnecrypted);
                if (options.type === DataTypes.JSONB || options.type === DataTypes.JSON) {
                    return JSON.parse(decryptedData);
                } 
                return decryptedData;
            },
            set (value: string) {
                let parsedValue = value;
                if (options.type === DataTypes.JSONB || options.type === DataTypes.JSON) {
                    parsedValue = JSON.stringify(value);
                }
                const encryptedData = encrypt(secret, parsedValue);
                this.setDataValue(options.field, encryptedData);
            }
        };
    }

    return field;

};

export const decrypt = (secret: string, rawData: string, returnRawOnUnecrypted: boolean = false): string => {
    try {
        const [rawValue, initVector] = rawData.split(ivSeparator);
        const decipher = crypto.createDecipheriv(algorithm, secret, Buffer.from(initVector, 'base64'));
        let decryptedData = decipher.update(rawValue, 'base64', 'utf-8');
        decryptedData += decipher.final('utf-8');
        return decryptedData;
    } catch (err) {
        if (returnRawOnUnecrypted) {
            return rawData;
        } else {
            throw err;
        }
    }
};

export const encrypt = (secret: string, value: string): string => {
    const initVector = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, secret, initVector);
    let encryptedData: string = cipher.update(value, 'utf-8', 'base64');
    encryptedData += cipher.final('base64');
    return `${encryptedData}${ivSeparator}${initVector.toString('base64')}`;
};

export interface ModelAttributeColumnOptionsWithName extends ModelAttributeColumnOptions {
    field: string,
    baseType?: DataTypes.DataTypeAbstract,
    returnRawOnUnecrypted?: boolean,
};
