import { Sequelize } from 'sequelize';

export const getSequlize = async (storage: string = ':memory:'): Promise<Sequelize> => {
    const sequelize = new Sequelize({
        dialect: 'sqlite',
        storage,
        define: {
            timestamps: true,
        },
        logging: false,
    });
    // await sequelize.authenticate();
    return sequelize;
};

