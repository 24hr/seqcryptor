import { DataTypes, Model, Sequelize } from 'sequelize';

import { getSequlize } from './db';

import { seqCryptor } from '../';

describe('Encrypt', () => {

    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = await getSequlize();
    });

    it('Can create a text field that is encryted', async () => {

        const encryptor = seqCryptor('_SUPER_SECRET_KEY_WITH_32_BYTES_');

        class User extends Model {
            public name!: string;
            public guid!: string;
        }

        User.init({
            guid: {
                allowNull: false,
                unique: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
            },
            name: encryptor({
                type: DataTypes.STRING,
                field: 'name',
            }),
        }, {
            sequelize,
            paranoid: false,
        });

        await User.sync();

        const user = await User.create({ name: 'bar' });

        const userRaw = await User.findOne({ where: { guid: user.guid }, raw: true });
        expect(userRaw!.name).not.toEqual('bar');

        const userParsed = await User.findOne({ where: { guid: user.guid } });
        expect(userParsed!.name).toEqual('bar');

    });

    it('Can create a JSONB field that is encryted', async () => {

        const encryptor = seqCryptor('datamaskinWzA1s7j44tNxBLiA3pMShH');

        type Info = {
            foo: number,
            bar: string,
        }

        class User extends Model {
            public info!: Info;
            public guid!: string;
        }

        User.init({
            guid: {
                allowNull: false,
                unique: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
            },
            info: encryptor({
                type: DataTypes.JSONB,
                allowNull: true,
                field: 'info',
            }),
        }, {
            sequelize,
            paranoid: false,
        });

        await User.sync();

        const user = await User.create({ 
            info: {
                foo: 1,
                bar: 'hej',
            } 
        });

        const userRaw = await User.findOne({ where: { guid: user.guid }, raw: true });
        expect(userRaw!.info).not.toEqual(JSON.stringify((user as any).dataValues.info));

        const userParsed = await User.findOne({ where: { guid: user.guid } });

        expect(userParsed!.info.foo).toEqual(1);
        expect(userParsed!.info.bar).toEqual('hej');

    });

    it('Can create a JSON field that is encryted', async () => {

        const encryptor = seqCryptor('datamaskinWzA1s7j44tNxBLiA3pMShH');

        type Info = {
            foo: number,
            bar: string,
        }

        class User extends Model {
            public info!: Info;
            public guid!: string;
        }

        User.init({
            guid: {
                allowNull: false,
                unique: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
            },
            info: encryptor({
                type: DataTypes.JSON,
                allowNull: true,
                field: 'info',
            }),
        }, {
            sequelize,
            paranoid: false,
        });

        await User.sync();

        const user = await User.create({ 
            info: {
                foo: 1,
                bar: 'hej',
            } 
        });

        const userRaw = await User.findOne({ where: { guid: user.guid }, raw: true });
        expect(userRaw!.info).not.toEqual(JSON.stringify((user as any).dataValues.info));

        const userParsed = await User.findOne({ where: { guid: user.guid } });

        expect(userParsed!.info.foo).toEqual(1);
        expect(userParsed!.info.bar).toEqual('hej');

    });




});



