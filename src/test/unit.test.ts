import { DataTypes, Model, Sequelize } from 'sequelize';

import { encrypt, decrypt } from '../';

describe('Encrypt', () => {

    it('Can encrypt', async () => {
        const encrypted = encrypt('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH', 'foo');
        expect(encrypted).not.toEqual('foo');
    });

    it('Can decrypt', async () => {
        const encrypted = encrypt('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH', 'foo');
        const decrypted = decrypt('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH', encrypted);
        expect(decrypted).toEqual('foo');
    });

    it('Decrypt with wrong secret doesnt work', async () => {
        const encrypted = encrypt('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH', 'foo');
        const decrypted = () => decrypt('XP84jM8C8gWzA1s7j44tNxBLiA3pMShH', encrypted);
        expect(decrypted).toThrow();
    });

    it('Uncrypted data throws an error', async () => {
        const run = () => decrypt('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH', 'hejhopp');
        expect(run).toThrow();
    });

    it('Uncrypted data is returned as it is, if flag is provided', async () => {
        const decrypted = decrypt('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH', 'hejhopp', true);
        expect(decrypted).toEqual('hejhopp');
    });


});



