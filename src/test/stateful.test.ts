import { DataTypes, Model, Sequelize } from 'sequelize';

import { getSequlize } from './db';

import { seqCryptor } from '../';

import path from 'path';

describe('Stateful', () => {

    // This test makes sure that even if we restart a new instance the encryption is decriptable
    it('An ecrypted value is correclty decrypted even if the sequelize is reinitiated', async () => {

        // FIRST set
        const sequelize = await getSequlize(path.join(__dirname, './data/data.sqllite'));
        const encryptor = seqCryptor('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH');

        class User extends Model {
            public name!: string;
            public guid!: string;
        }

        const model = (encryptor: any): any => {
            return {
                guid: {
                    allowNull: false,
                    unique: true,
                    type: DataTypes.UUID,
                    defaultValue: DataTypes.UUIDV4,
                },
                name: encryptor({
                    type: DataTypes.STRING,
                    field: 'name',
                }),
            };
        };

        User.init(model(encryptor), {
            sequelize,
            paranoid: false,
        });

        await User.sync();

        const user = await User.create({ name: 'bar' });

        await sequelize.close();

        // SECOND Set
        const sequelize2 = await getSequlize(path.join(__dirname, './data/data.sqllite'));
        const encryptor2 = seqCryptor('hP84jM8C8gWzA1s7j44tNxBLiA3pMShH');

        User.init(model(encryptor2), {
            sequelize: sequelize2,
            paranoid: false,
        });

        await User.sync();

        const users = await User.findAll();
        const userParsed = await User.findOne({ where: { guid: user.guid } });
        expect(userParsed!.name).toEqual('bar');

    });

});



